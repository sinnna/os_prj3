#include <iostream>
#include <vector>
#include <thread>
#include <string.h>
#include <algorithm>
#include <semaphore.h>
#include <fstream>
#include <ctime>
#include <sys/time.h>

#define MAX 10000000

using namespace std;

void parse_cmd(string a, vector<string> &b, char delim)
{
	string temp;
	for (int i = 0; i < a.size(); ++i)
		if(a[i] != delim)
			temp += a[i];		
	b.push_back(temp);
}

vector <string> parse_in(string a, char delim)
{
	vector<string> b; 
	for (int i = 0; i < a.size(); ++i)
		if(a[i] != delim)
			b.push_back(string(1, (a[i])));	
	return b;
}

class Monitor_all
{
public:
	Monitor_all(int init){all_sum = init; sem_init(&final_sum, 0, 1);}
	void add_n(float a){
		sem_wait(&final_sum);
		all_sum += a;
		sem_post(&final_sum);
	}
	float all_sum;
private:
	sem_t final_sum;
};

Monitor_all sum_monitor = Monitor_all(0);

class Monitor_path
{
public:
	Monitor_path(){sem_init(&mutex, 0, 1);}
	float move_car(int h, int p);
private:
	sem_t mutex;
};
float Monitor_path::move_car(int h, int p)
{
	sem_wait(&mutex);
	float sums = 0;
	for (int i = 0; i < MAX; i++)
		sums += i/(1000000.0 * p * h);
	sum_monitor.add_n(sums);
	sem_post(&mutex);
	return sums;
}

int find_monitor(string path, vector <string> all_paths)
{

	for (int i = 0; i < all_paths.size(); ++i)
		if(path[0] == all_paths[i][0])
			return i;
	return -1;
}

void take_path(int p, vector<string> all_paths,  vector <string> path_to_take, vector<Monitor_path> &all_monitors, int path_num, int car_num)
{
	struct timeval start_time;
	ofstream myfile;
	string name = to_string(path_num + 1) + to_string(car_num + 1) + ".txt";
	myfile.open(name);	
	for (int i = 0; i < path_to_take.size() - 1; i++)
	{
		int index = find_monitor(path_to_take[i], all_paths);
		gettimeofday(&start_time, NULL);
		myfile << path_to_take[i] << "," << start_time.tv_sec* 1000 + start_time.tv_usec/1000 << "," << path_to_take[i+1] << ",";
		float this_emit = all_monitors[index].move_car(all_paths[index][2], p);
		gettimeofday(&start_time, NULL);
		myfile << start_time.tv_sec* 1000 + start_time.tv_usec/1000 << "," << this_emit << "," << sum_monitor.all_sum << endl;
	}
}

int main()
{
	string temp;
	vector <string> parsed_in;
	vector <vector <string>> parsed_path;
	vector <int> num_cars;
	vector <thread> all_threads;
	vector <Monitor_path> all_monitors;
	while(getline(cin, temp))
	{
		if(temp == "#")
			break;
		parse_cmd(temp, parsed_in, '-');
		all_monitors.push_back(Monitor_path());//creating the monitor for each path
	}

	while(getline(cin, temp))
	{
		parsed_path.push_back(parse_in(temp, '-'));
		getline(cin, temp);
		num_cars.push_back(stoi(temp));
	}

	for (int i = 0; i < num_cars.size(); ++i)
	{
		for (int j = 0; j < num_cars[i]; j++)
		{
			int p = rand() % 10;
			thread car(take_path, p, parsed_in, parsed_path[i], ref(all_monitors), i, j);
			all_threads.push_back(move(car));
		}
	}

	for (int i = 0; i < all_threads.size(); ++i)
		all_threads[i].join();

}